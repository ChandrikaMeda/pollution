# Reads the input data file and store ot in the variable read_file
read_file = open("../data/AtchisonUV_20150801_to_20151119.csv","r")

#Writes the file to the output file
write_file = open("out.txt", "w")
months = {}
# This line of code reads each line in the input "read_file"
for line in read_file.readlines()[1:]:

    data = line.strip().split(",")
#checks the length of the data is equal to 10
    if len(data) == 10:
        Date,Benzene,CS2,Ozone,SO2,Toluene,Xylene,WindDirection,WindSpeed,WindOrigin = data
# to check whether the month in the date is a single value or double value that is for months like october with "10" it takes 
#directly takes but for months like september with just single digit "9" it adds "0" at the beginning.
        if len(Date.split('/')[0]) == 2:
            dateCust = Date.split('/')[0]
        else:
            dateCust = "0" + Date.split('/')[0]

        if dateCust == "08":
            dateCust = "August"
        elif dateCust == "09":
            dateCust = "September"
        elif dateCust == "10":
            dateCust = "October"
        elif dateCust == "11":
            dateCust = "November"
        write_file.write(dateCust + "\t" + str(Xylene) + "\n")
        
# closes the input read_file and output write_file
read_file.close()
write_file.close()