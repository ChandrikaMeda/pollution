# Reads the file to be sorted and stores it as unsorted
unsorted = open("out.txt", "r")
#Writes the sorted output file to the sorted.
sorted = open("sorted.txt", "w")

#reads all the lines from the unsorted file and sorts it using the natural sorting
data = unsorted.readlines()
data.sort()

# for each line in the datalist sorts the input and writes it to the "sorted" file.
for line in data:
    sorted.write(line)

#closes the input "unsorted" and output "sorted" files
unsorted.close()
sorted.close()