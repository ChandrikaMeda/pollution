# Reads the input data file and store it in the variable read_file
input = open("sorted.txt","r")
#Writes the file to the output file
output = open("result.txt", "w")
#Initializes the value for thiskey as equal to "null" and "0.0"
thisKey = ""
thisValue = 0.0
# initilaizes the value of count to be "0"
count = 0

# for each line in the input file it split the elements by
for line in input:
  data = line.strip().split('\t')
  month, value = data  

# to check if the thiskey value is equal to month or not
  if month != thisKey:
    if thisKey:
      
      output.write('Month: ' + str(thisKey) + '\t Average of Xylene: ' + str(sum/count) + '\n')
  # initializes the value of thiskey to be eqaul to month ,thisValue as "0.0" ,sum as 0,count as 0.
    thisKey = month
    thisValue = 0.0
    sum = 0
    count = 0
  
  # Adds the value to the thisValue variable and keeps adding the sum for every increment in the count.
  thisValue += float(value) 
  sum += float(value)
  count += 1

# Writes the output in the below format as "Month: 10" Average of xylene is ********
output.write('Month: ' + str(thisKey) + '\t Average of Xylene: ' + str(sum/count) + '\n')

# closes the input and output files
input.close()
output.close()