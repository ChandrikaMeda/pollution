# Pollution Analyser
 
 --------
Course : 44-564_01   Design of Data-Intensive Systems

### Team - 1D
#### Developer Pairs:

* Pair 01 - Supraja Kumbam, Jyothsna Pala
    1. Supraja Kumbam:
            * Email ID: S530862@nwmissouri.edu
            * Course : Applies Computer Science
            * Semester : 02

    2. Jyothsna Pala: 
            * Email ID: S528755@nwmissouri.edu
            * Course : Applied Computer Science
            * Semester : 04

* Pair 02 - Chandrika Meda, Sreelakshmi Onteddu

    1. Chandrika Meda:
            * Email ID: S530476@nwmissouri.edu
            * Course : Applied Computer Science
            * Semester : 02

    2. Srilakshmi Onteddu: 
            * Email ID: S530746@nwmissouri.edu
            * Course :Applied Computer Science
            * Semester : 02

### Overview of project: 
The  project is all about to develop a map reduce program to analyze the different pollutant levels in the atmosphere.
We have dataset which includes different pollutants for each particualr date and time.Data from refinery monitoring stations in Atchison Village, Richmond CA, United States. This is just a small piece of the data available. 
I hope to get more data collected in the future.

###Data Source:
* link: https://www.kaggle.com/nicapotato/pollution-in-atchison-village-richmond-ca/data
* No of Records : 31,643 from August to November 2015
* No of Records : 31,643
* Size : 1.48 MB
* File format : CSV
* Data Format : Structured

###Links: 
* [https://bitbucket.org/ChandrikaMeda/pollution/overview](https://bitbucket.org/ChandrikaMeda/pollution/overview)
* [https://bitbucket.org/ChandrikaMeda/pollution/issues?status=new&status=open](https://bitbucket.org/ChandrikaMeda/pollution/issues?status=new&status=open)

### Big data Qualifications/challanges
* Value:It is useful for the people to follow the type of chemicals that are released into air and is also useful for analysis purpose.
* Volume:The Data set contains the list of chemicals that pollute the air in the Atchison Village from August to November 2015 for different time. They are total of 31643 records.
* Veracity: It gives us the statistical results for every chemical during different times and it is trust worthy even though published in different websites. But, most of the times the results were published by the officers.
* Variety: The data is structured and this data is in the forms of characters and integer values
* Velocity:The velocity for this will vary for every minute and type of chemical released into air in that particular period of time.

### Big Data Questions:
* 1) For each month, calculate the average level of xylene? - Jyothsna Pala
* 2) For each month, calculate the average wind direction? - Chandrika Meda
* 3) For each month, calculate the speed of the wind? - Sreelakshmi Onteddu
* 4) For each data, calculate the highest pollutant? - Supraja Kumbbam

### Big Data Solutions:

1. For each month, Calculate the average level of the xylene?

    #### Story : 
    Our objective is to analyse the data related to the pollution statistics in a village. In partiular, I have chosen to calculate the average level of the xylene pollutant for eah month among the over all pollutants.To achieve this I have designed a mapper which which lists the data of the particular date and the xylene value on that particular date.I have sorted the data using the alphabetical sorting and generated a sorted output.From the sorter output I have calculated the average value of the xylene for each month using the reducer.At the end with average values from the reducer outputs I was able to generate a Bar Graph for average values on each month.
    
    #### Data Flow :
    * Mapper input:8/28/2015 14:45	2.5	2.5	12.16	2.5	2.5	5.34	192	7	SSW
    * Mapper output / Reducer input:  Intermediate key-value pair :8/28/2015 14:55   5.34(xylene)
    * Reducer output:  Xylene(average):2.767552369 (count:8927, sum:24705.94)
    * Chart Type: Bar graph
    * Language: I have used python to perform the MapReduce.

    #### Reducer Output Results:

    | Month calculated | Average Value of Xylene |
    ------------- | -------------------------------------
    | Month: August	 | Average of Xylene: 2.76755236922 |
    | Month: November | Average of Xylene: 2.5 |
    | Month: October | Average of Xylene: 3.05958309985 |
    | Month: September | Average of Xylene: 2.53021830005 |

    #### Link for the screenshot image:
    ![Q1_Bargraph.PNG] (images/Q1_Bargraph.PNG)



2. For each month, calculte the average wind direction?

    #### Story : 
    The main objective is to analyse the data related to the pollution statistics in a village. In partiular, I have chosen to calculate the average wind direction for a particular month collectively.I have written a mapper program which lists the data of the particular date and the direction of the wind on that particular date and even the month.The sorting of the data is done using the alphabetical order an then the sorted data is obtained.From the sorted data I have obtained the output for the reducer program. From the obtained output I have generated a bar graph to represent the direction of the wind.


    ####Data Flow
    * Mapper input: 10/10/2015 4:30, 2.5, 2.5, 2.5, 2.5, 573.36, 144.61, 166, 5, SSE
    * Mapper output / Reducer input: Intermediate Key-value pair : 09, 352
    * Reducer output:Month: 10	Average Direction Wind: 160.795217245
    * Language:  I will be using python to finish the mapper- reducer program
    * Kind of chart: bar chart(month vs wind direction)

    ####Reduced Output Results:
     | Month calculated | Average Value of Wind Direction |
    ------------- | -------------------------------------
    | Month: 08 | 	Average Direction Wind: 177.851636038 |
    | Month: 09 |   Average Direction Wind: 166.479851353 |
    | Month: 10 |   Average Direction Wind: 160.795217245 |
    | Month: 11 |   Average Direction Wind: 191.879899595|
    
    #### Link for the screenshot image:
    ![Direction.PNG] (images/Direction.PNG)


3. For each month, to calculate the speed of the wind?

    #### Story : 
    Our objective is to analyse the data related to the pollution statistics in a village. In partiular, I have chosen to calculate the average level of wind speed  for eah month among the over all pollutants.To achieve this I have designed a mapper which gives the result of particular date and the speed  on that particular date.I have sorted with the help of  alphabetical sorting and generated a sorted output. From this output I have calculated the average value of  wind speed for each month using the reducer.From the obtained output I have generated a Bar Graph for average values on each month. Dataset contains months from only 8 to 11.

    #### Data Flow :
    * Mapper input:10/13/15 16:00	2.5	2.5	117.68	2.5	37.36	7.7	176	8	S
    * Intermediate key-value pair :10
    * Reducer Output:  Month: 10	Average Wind Speed: 5.43987874705
    * Language:  I will use  python to finish the mapper- reducer program
    * Kind of chart: Bar chart(month vs wind speed)
     
    #### Reducer Output Results:

    | Month  | Average Wind Speed |
    ------------- | -------------------------------------
    | Month: 08 | Average Wind Speed: 7.57821604662 |
    | Month: 09 | Average Wind Speed: 6.85564975032 |
    | Month: 10 | Average Wind Speed: 5.43987874705 |
    | Month: 11 | Average Wind Speed: 7.24522108515 |

    #### Link for the screenshot image:
    ![Speed_barGraph.PNG] (images/Speed_barGraph.PNG)

4. For a particular month, what is the sum of all the chemicals?

    #### Story : 
    Our objective is to analyse the data related to the pollution statistics in a village. In partiular, I have chosen to calculate the average of the all the pollutants for each month.I have done this by creating a mapper which lists the data of the particular date and all the pollutant values present on that date.The sorting of the data has been done by using the alphabetical order. From the output obtained from the sort I have got the calculations for the sum of all the pollutants for every month, by reducing the data.With the output values obtained from the reducer I have generated the pie chart for the data source.

    #### Data Flow :
    * Mapper input: 10/10/2015 3:15	2.5	2.5	2.5	2.5	2.5	220.61	162	6	SSE

    * Mapper output / Reducer input:  Intermediate key-value pair :	08 2.5(Benzene) 2.5(CS2) 10(Ozone) 2.5(S02) 2.5(Toluene) 2.5(Xylene)

    * Reducer output:  Month: 8(Key)	Benzene: 22310	CS2: 22310	Ozone: 144105.69	SO2: 22927.22	Toluene: 22456.07	Xylene: 22315.56(Values)

    * Chart Type: Pie Chart

    * Language: I have used python to perform the MapReduce.
    
    #### Reducer Output Results:
	
    | Month calculated| Benzene  | CS2       | Ozone     | S02      | Toluene  | Xylene   |
    ------------------|----------|-----------|-----------|----------|----------|----------|-----
    | Month: 8	      | 22310    | 144105.69 | 22927.22  | 22456.07 | 22315.56 | 22315.56 |
    | Month: 9        | 21535.27 | 21527.5   | 299134.63 | 27786.58 | 26206.15 | 21787.74 |
    | Month: 10       | 22305.17 | 22267.5   | 269705.1  | 42778.53 | 27517.05 | 23740.03 |
    | Month: 11       | 12947.5  | 12947.5   | 149695.53 | 18665.46 | 13013.48 | 12947.5  |

    #### Link for the screenshot image:
    ![Capture1.PNG] (images/Capture1.PNG)
    