input=open("aftersort_kumbam.txt","r")
output=open("afterreducer_kumbam.txt","w")
thisKey = ""
counter = 0
ben=cs2=oz=so2=tol=xyl=0
# output.write("Month"  + "\tBenzene" +  "\t\t\tCS2" +  "\t\t\tOzone" +"\t\t\tSO2" + "\t\t\tToluene" + "\t\tXylene" +"\n")
output.write("{0:^10}".format(" ")  + "{0:^10}".format("Benzene") +  "{0:^10}".format("CS2") +  "{0:^10}".format("Ozone") +"{0:^10}".format("SO2") + "{0:^10}".format("Toluene") + "{0:^10}".format("Xylene") +"\n")
# output.write("Month"  + "\tTotal Benzene" +  "\tTotal CS2" +  "\tTotal Ozone" +"\tTOtal SO2:" + "\tTotal Toluene:" + "\tTOtal Xylene:" +"\n")
for line in input:
    data = line.strip().split(',')
    month,Benzene,CS2,Ozone,SO2,Toluene,Xylene = data
    
    if month != thisKey:
        
        if thisKey:
            output.write("{0:^10}".format("Month: "+thisKey) +"{0:^10}".format(str(ben*1.0))+  "{0:^10}".format(str(cs2*1.0))+ "{0:^10}".format(str(oz*1.0))+ "{0:^10}".format(str(so2*1.0))+ "{0:^10}".format(str(tol*1.0))+ "{0:^10}".format(str(xyl*1.0))+"\n")
        thisKey = month 
        ben=cs2=oz=so2=tol=xyl=0
    ben += float(Benzene)
    cs2+=float(CS2)
    oz+=float(Ozone)
    so2+=float(SO2)
    tol+=float(Toluene)
    xyl+=float(Xylene)

output.write("{0:^10}".format("Month: "+thisKey) +"{0:^10}".format(str(ben*1.0))+  "{0:^10}".format(str(cs2*1.0))+ "{0:^10}".format(str(oz*1.0))+ "{0:^10}".format(str(so2*1.0))+ "{0:^10}".format(str(tol*1.0))+ "{0:^10}".format(str(xyl*1.0))+"\n")

input.close()
output.close()