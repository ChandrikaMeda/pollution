#Reading the content of the file
s = open("sort_output.txt","r")
r = open("red_output.txt", "w")

thisKey = ""
counter = 0
sum = 0
for line in s:
    data = line.strip().split(',')
    month,wind_val = data

    if month != thisKey:
        if thisKey:
            r.write("Month: " +thisKey + '\tAverage Wind Speed: ' + str((sum*1.0)/counter)+'\n')

        thisKey = month 
        counter = 0
        sum = 0
    sum += int(wind_val)
    counter += 1

r.write("Month: " +thisKey + '\t Average Wind Speed: ' + str((sum*1.0)/counter)+'\n')

s.close()
r.close()