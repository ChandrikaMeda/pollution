#Reading the content of the file
input = open("../data/data.csv","r")
output = open("map_output.txt","w")
counter = 0;
for line in input : 
    data = line.strip().split(",")
    Date,Benzene,CS2,Ozone,SO2,Toluene,Xylene,Wind_Direction,Wind_Speed,Wind_Origin = data
    tempDate = Date.split("/")
    
    if Wind_Speed != "" and counter > 0:
        if len(tempDate[0]) == 1:
            output.write( "0"+tempDate[0] + "," + Wind_Speed + "\n")
        else:
            output.write(tempDate[0] + "," + Wind_Speed + "\n")
    counter += 1;
#Close the opened files
input.close()
output.close()